import express from "express";
import {
  reset,
  getAll,
  getProfile,
  updateUser,
  getFriends,
} from "../controllers/userController.js";
import {
  adminAuth,
  userAuth,
  register,
  login,
} from "../controllers/authController.js";
let router = express.Router();
export default router;

router.post("/register", async (req, res) => {
  let result = await register(req);
  res.send(result);
});
router.post("/login", async (req, res) => {
  let result = await login(req);
  res.send(result);
});

router.delete("/reset", adminAuth, async (req, res) => {
  console.log("admin trying to delete all");
  result = await reset(req);
  res.send(result);
});

router.get("/", userAuth, async (req, res) => {
  let result = await getAll();

  res.send({
    message: "successfully got list of users",
    payload: res.payload,
    data: result,
  });
  res.send();
});

router.get("/find", async (req, res) => {
  result = await getProfile(req.query);
  res.send(result);
});

router.put("/update", adminAuth, async (req, res) => {
  result = await updateUser(req);
  res.send(result);
});

router.delete("/delete", adminAuth, async (req, res) => {
  console.log("admin trying to delete a user");
  result = await delete req.query;
  res.send(result);
});

router.put("/setadmin", adminAuth, async (req, res) => {
  let filter = req.body;
  req.body.filter = filter;
  req.body.update = { isAdmin: true };
  result = await updateUser(req, res);
  res.send(result);
});

// deprecated
router.get("/:username", async (req, res) => {
  result = await getProfile(req.params.username);
  res.send(result);
});

// deprecated
router.get("/:username/friends", async (req, res) => {
  result = await getFriends(req.params.username);
  res.send(result);
});
