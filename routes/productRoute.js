import express from "express";
import {
  getAll,
  getAll2,
  createNew,
  edit,
  checkout,
  orders,
} from "../controllers/productController.js";
import {
  nonAdminAuth,
  adminAuth,
  userAuth,
  register,
  login,
} from "../controllers/authController.js";
let router = express.Router();
export default router;

router.get("/", async (req, res) => {
  let result = await getAll();

  res.send({
    message: "successfully got list of products",
    payload: res.payload,
    data: result,
  });
});
router.get("/getagain", userAuth, async (req, res) => {
  let result = await getAll2(req, res);

  res.send({
    message: "successfully got list of products",
    payload: res.payload,
    data: result,
  });
});

router.post("/sell", adminAuth, async (req, res) => {
  let result = await createNew(req);
  res.send(result);
});

router.post("/edit", adminAuth, async (req, res) => {
  let result = await edit(req);
  res.send(result);
});
router.post("/checkout", nonAdminAuth, async (req, res) => {
  let result = await checkout(req);
  res.send(result);
});
router.post("/orders", nonAdminAuth, async (req, res) => {
  let result = await orders(req, res);
  res.send(result);
});

router.get("/find", async (req, res) => {
  result = await getProfile(req.query);
  res.send(result);
});

router.put("/update", adminAuth, async (req, res) => {
  result = await updateUser(req);
  res.send(result);
});

router.delete("/delete", adminAuth, async (req, res) => {
  console.log("admin trying to delete a user");
  result = await delete req.query;
  res.send(result);
});

router.put("/setadmin", adminAuth, async (req, res) => {
  let filter = req.body;
  req.body.filter = filter;
  req.body.update = { isAdmin: true };
  result = await updateUser(req, res);
  res.send(result);
});

// deprecated
// router.get("/:username", async (req, res) => {
//   result = await getProfile(req.params.username);
//   res.send(result);
// });

// deprecated
router.get("/:username/friends", async (req, res) => {
  result = await getFriends(req.params.username);
  res.send(result);
});
