// authController

import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import prisma from "../prisma/prisma.js";

export async function register(req, res, next) {
  console.log(req.body);
  const { username, password, email, mobileNo, address, isAdmin } = req.body;
  if (password.length < 6) {
    return JSON.stringify({
      message: "Password less than 6 characters",
    });
  } else if (
    (await prisma.user.findMany({ where: { email: email } })).length > 0
  ) {
    return JSON.stringify({
      message: "Email account already exists",
    });
  }
  try {
    console.log("im trying to create");
    console.log(bcrypt.hashSync(req.body.password, 10));
    let result = await prisma.user.create({
      data: {
        username: username,
        email: email,
        password: bcrypt.hashSync(req.body.password, 10),
        isAdmin: isAdmin === "true",
        phone: mobileNo,
        address: address,
        role: "user",
      },
    });

    req.body.password = "";
    return {
      message: "User successfully created",
      data: result,
    };
  } catch (err) {
    JSON.stringify({
      message: "User not successful created",
      error: err.message,
    });
  }
}

export async function login(req, res, next) {
  try {
    const user = await prisma.user.findUnique({
      where: {
        email: req.body.email,
      },
    });
    if (!user) {
      return JSON.stringify({
        message: "Login not successful",
        error: "User not found",
      });
    } else {
      const maxAge = 3 * 60 * 60; // 3 hours in seconds
      let passwordCheck = bcrypt.compareSync(req.body.password, user.password);
      if (passwordCheck) {
        let payload = {
          email: user.email,
          isAdmin: user.isAdmin,
          id: user.id,
          role: user.role,
          image: user.image,
        };
        const accessToken = jwt.sign(payload, process.env.ACCESS_TOKEN_SECRET, {
          expiresIn: maxAge,
        });
        // if (!res.cookie) res.cookie = {};
        // console.log(res.cookie);

        // res.cookie("jwt", accessToken, {
        //   httpOnly: false,
        //   maxAge: maxAge * 1000, // 3hrs in ms
        // });
        // no more cookie

        return {
          message: "logged in successfully",
          email: user.email,
          token: accessToken,
        };
      } else {
        return JSON.stringify({
          message: "Login not successful",
          error: "Password incorrect",
        });
      }
    }
  } catch (error) {
    return JSON.stringify({
      message: "An error occurred",
      error: error.message,
    });
  }
}

export async function adminAuth(req, res, next) {
  console.log("admin auth was called");
  let token = req.headers.authorization;
  if (token) {
    token = token.slice(7, token.length);
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decodedToken) => {
      if (err) {
        console.log("is it here");
        return res.status(401).json({ message: "Not authorized" });
      } else {
        if (!decodedToken.isAdmin) {
          console.log("or here");
          return res.status(401).json({ message: "Not authorized" });
        } else {
          res.payload = decodedToken;
          next();
        }
      }
    });
  } else {
    return res
      .status(401)
      .json({ message: "Not authorized, token not available" });
  }
}

export async function nonAdminAuth(req, res, next) {
  let token = req.headers.authorization;
  if (token) {
    token = token.slice(7, token.length);
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decodedToken) => {
      if (err) {
        return res.status(401).json({ message: "Not authorized" });
      } else {
        if (decodedToken.isAdmin) {
          return res.status(401).json({ message: "Not authorized" });
        } else {
          res.payload = decodedToken;
          next();
        }
      }
    });
  } else {
    return res
      .status(401)
      .json({ message: "Not authorized, token not available" });
  }
}

export async function userAuth(req, res, next) {
  let token = req.headers.authorization;
  if (token) {
    token = token.slice(7, token.length);
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decodedToken) => {
      if (err) {
        return res.status(401).json({ message: "Not authorized" });
      } else {
        console.log(decodedToken);
        res.payload = decodedToken;
        // res.write(JSON.stringify({ payload: decodedToken }));
        next();
      }
    });
  } else {
    return res
      .status(401)
      .json({ message: "Not authorized, token not available" });
  }
}
