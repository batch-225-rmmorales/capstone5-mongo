// try mo nga ilabas ung logic sa functions... para pde i call na lang

// controllers contain the functions and business logic of our express JS meaning all the operation it can do will be placed here

// const User = require("../models/user");
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";

// const ACCESS_TOKEN_SECRET = require("../password").ACCESS_TOKEN_SECRET;
import dotenv from "dotenv";
import prisma from "../prisma/prisma.js";
dotenv.config();

export async function getAll() {
  // if (res.payload.role == "user") {
  //   return await prisma.product.findMany({ data: { isActive: true } });
  // } else if (res.payload.role == "admin") {
  //   return await prisma.product.findMany({});
  // }
  return await prisma.product.findMany({});
}

export async function orders(req, res) {
  // if (res.payload.role == "user") {
  //   return await prisma.product.findMany({ data: { isActive: true } });
  // } else if (res.payload.role == "admin") {
  //   return await prisma.product.findMany({});
  // }
  return await prisma.cart.findMany({
    where: {
      buyer: res.payload.email,
    },
  });
}
export async function getAll2(req, res) {
  console.log("yo");
  console.log(req.query);
  if (req.query.item) {
    return await prisma.product.findUnique({ where: { id: req.query.item } });
  } else if (res.payload.role == "user") {
    console.log("im here");
    return await prisma.product.findMany({ where: { isActive: true } });
  } else if (res.payload.role == "admin") {
    console.log("im here pala");
    return await prisma.product.findMany({});
  }
}
export async function createNew(req, res) {
  // let bodyParsed = JSON.parse(req.body);
  req.body.price = Number(req.body.price);
  req.body.stock = Number(req.body.stock);
  let product = await prisma.product.create({ data: req.body });
  return await { message: "product successfully created", data: product };
}
export async function checkout(req, res) {
  req.body.products[0].price = Number(req.body.products[0].price);
  req.body.products[0].stock = Number(req.body.products[0].stock);
  // let bodyParsed = JSON.parse(req.body);
  let findProduct = await prisma.product.findUnique({
    where: {
      id: req.body.products[0].id,
    },
  });
  let newStock = findProduct.stock - req.body.products[0].quantity;
  findProduct = await prisma.product.update({
    where: {
      id: req.body.products[0].id,
    },
    data: {
      stock: newStock,
    },
  });
  console.log(req.body);
  console.log(req.body.products);
  const cart = await prisma.cart.create({
    data: {
      seller: "admin",
      buyer: req.body.buyer,
      totalProducts: 1,
      total:
        Number(req.body.products[0].price) *
        Number(req.body.products[0].quantity),
      totalQuantity: Number(req.body.products[0].quantity),

      products: req.body.products,
      isPaid: true,
    },
  });
  return await { message: "product successfully checked out", data: cart };
}

export async function getOne(req, res) {
  return await prisma.product.findMany({ where: { id: req.query.item } });
}

export async function edit(req, res) {
  // let bodyParsed = JSON.parse(req.body);
  if (req.body.update.price) {
    req.body.update.price = Number(req.body.update.price);
  }
  if (req.body.update.stock) {
    req.body.update.stock = Number(req.body.update.stock);
  }

  let product = await prisma.product.update({
    where: req.body.filter,
    data: req.body.update,
  });
  return await { message: "product successfully updated", data: product };
}

// module.exports.findEmail = async (req) => {
//   return await User.find({ email: req.body.email });
// };

// module.exports.deleteByName = async (req) => {
//   let toDelete = await Task.findOne({ name: req.body.name });
//   return await toDelete.remove();
// };

// module.exports.deleteByID = async (taskID) => {
//   let toDelete = await Task.findOne({ _id: taskID });
//   return await toDelete.remove();
// };

export async function updateUser(req) {
  let toUpdate = await User.findOne(req.body.filter);
  return await toUpdate.update(req.body.update);
}
export async function setadmin(req, res) {
  let toUpdate = await User.findOne(req.body);
  return await toUpdate.update({ isAdmin: true });
}

// module.exports.updateByID = async (taskID, req) => {
//   let toUpdate = await Task.findOne({ _id: taskID });
//   console.log(req.body.update);
//   await toUpdate.update(req.body.update);
//   return toUpdate.save();
// };
