import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

const modelNames = ["user"];
// const reset = async () => {
//   await prisma.user.deleteMany({});
// };

// reset();
// let data = [
//   { password: "test1234", email: "bob@prisma.io" },
//   { password: "test1234", email: "yewande@prisma.io" },
//   { password: "test1234", email: "angelique@prisma.io" },
// ];
// let user;
// data.forEach(async (user) => {
//   user = await prisma.user.create({
//     data: user,
//   });
// });

const seedProduct = async (element) => {
  // const users = await prisma.user.findMany({});
  // var randomUser = users.users[Math.floor(Math.random() * users.users.length)];
  // console.log(randomUser.username);

  const product = await prisma.product.create({
    data: {
      title: element.title,
      description: element.description,
      price: Number(element.price) * 60,
      thumbnail: element.thumbnail,
      images: element.images,
      category: element.category,
      seller: "seller",
      stock: 10,
    },
  });
};
const seedProducts = async () => {
  const response = await fetch("https://dummyjson.com/products?limit=100");
  const data = await response.json();
  data.products.forEach((element) => {
    seedProduct(element);
  });
};
seedProducts();
